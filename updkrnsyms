#!/usr/bin/env bash

# updkrnsymlnks - update kernel symbolic-links
# I use SYSLINUX as a bootloader on my laptop, which unlike Grubby
# doesn't have an out-of-the-box way to generate the config with
# updated kernel and initrd values. The workaround is that my boot
# directory has sym-links to the vmlinuz and initrd files prefixed
# with "-current" and "-fallback". The SYSLINUX config points to
# these files instead. This script moves the symlinks to the so they
# point to the appropriate files after a kernel update.

# > MAIN SEQUENCE

# what's the newest kernel?
newest_ver=$(ls vmlinuz-[0-9.]* | cut -d '-' -f2- | sort -Vr | sed -n '1p')

# see if the symlinks are correct and exit if so
if [ $(readlink 'vmlinuz-current') == "vmlinuz-${newest_ver}" ]
then
  echo "Kernel symbolic-links are up-to-date"
  exit 0
fi

# we're lagging so update
echo -n 'Kernel symbolic-links are lagging, updating... '

# delete the fallbacks and rename the currents to fallback
rm *-fallback*
perl-rename 's/current/fallback/' *-current*

# link the new kernel items (except initramfs)
for kitem in vmlinuz config System.map
do
  ln -s ${kitem}-${newest_ver} ${kitem}-current
done

# link the initramfs
ln -s initramfs-${newest_ver}.img initramfs-current.img

# report
echo -e 'DONE!\n'
echo "Current kernel: $(readlink vmlinuz-current | cut -d '-' -f2-)"
echo "Fallback kernel: $(readlink vmlinuz-fallback | cut -d '-' -f2-)"
